import AuthLayout from "./AuthLayout";
import SocialLogin from "./SocialLogin";

const AuthPageContent = {
  AuthLayout: AuthLayout,
  SocialLogin: SocialLogin,
};

export default AuthPageContent;
